//
//  Filters.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 08/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import Foundation

struct Filters {
    var interior: [String] = [ConservationState.GOOD.rawValue, ConservationState.UNACCEPTABLE.rawValue]
    var exterior: [String] = [ConservationState.GOOD.rawValue, ConservationState.UNACCEPTABLE.rawValue]
}
