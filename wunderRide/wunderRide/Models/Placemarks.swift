//
//  Placemarks.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 06/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PlaceMarks {
    let address: String?
    let coordinates: Coordinates?
    let engineType: String?
    let exterior: ConservationState?
    let fuel: Int
    let interior: ConservationState?
    let name: String?
    let vin: String?

    init(json: JSON) {
        address = json[Keys.address.rawValue].string ?? ""
        coordinates = Coordinates.init(json: json[Keys.coordinates.rawValue])
        engineType = json[Keys.engineType.rawValue].string ?? ""
        exterior = ConservationState(rawValue: json[Keys.exterior.rawValue].string ?? "")
        fuel = json[Keys.fuel.rawValue].int ?? 0
        interior = ConservationState(rawValue: json[Keys.interior.rawValue].string ?? "")
        name = json[Keys.name.rawValue].string ?? ""
        vin = json[Keys.vin.rawValue].string ?? ""
    }
}

struct Coordinates {
    let latitude: Float?
    let longitude: Float?

    init(json: JSON) {
        guard let values = json.array else {
            latitude = nil
            longitude = nil
            return
        }
        latitude = values.count > 1 ? values[0].float : nil
        longitude = values.count > 1 ? values[1].float : nil
    }
}

enum ConservationState: String {
    case GOOD
    case UNACCEPTABLE
    case NONE = ""
}

enum Keys: String {
    case address = "address"
    case coordinates = "coordinates"
    case engineType = "engineType"
    case exterior = "exterior"
    case fuel = "fuel"
    case interior = "interior"
    case name = "name"
    case vin = "vin"
}
