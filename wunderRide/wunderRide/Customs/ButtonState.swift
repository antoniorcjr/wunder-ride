//
//  UIButtonState.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 08/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import UIKit

class ButtonState: UIButton {
    @IBInspectable var imageSelected: UIImage?
    @IBInspectable var imageDeselected: UIImage?

    var buttonState: Bool = true

    override func awakeFromNib() {
        super.awakeFromNib()
        self.addTarget(self, action: #selector(onPress), for: .touchUpInside)

        refreshButtonImage()
    }

    @objc func onPress() {
        buttonState = !buttonState
        refreshButtonImage()
    }

    func refreshButtonImage() {
        if buttonState {
            self.setBackgroundImage(imageSelected, for: .normal)
        } else {
            self.setBackgroundImage(imageDeselected, for: .normal)
        }
    }

    func getButtonState() -> Bool {
        return buttonState
    }

    func setState(buttonState: Bool) {
        self.buttonState = buttonState
        refreshButtonImage()
    }
}
