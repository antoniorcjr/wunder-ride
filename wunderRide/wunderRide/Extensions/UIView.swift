//
//  UIView.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 08/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        
        set {
            self.layer.cornerRadius = newValue
        }
    }
}
