//
//  CarTableViewCell.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 07/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import UIKit

class CarTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var interiorImage: UIImageView!
    @IBOutlet weak var exteriorImage: UIImageView!

    @IBOutlet weak var lowFuelView: UIView!
    @IBOutlet weak var mediumFuelView: UIView!
    @IBOutlet weak var highFuelView: UIView!

    @IBOutlet weak var lowFuelConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediumFuelConstraint: NSLayoutConstraint!
    @IBOutlet weak var highFuelConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func setInterior(value: String) {
        if value == "GOOD" {
            interiorImage.image = #imageLiteral(resourceName: "icon_good")
        } else {
            interiorImage.image = #imageLiteral(resourceName: "icon_bad")
        }
    }

    func setExterior(value: String) {
        if value == "GOOD" {
            exteriorImage.image = #imageLiteral(resourceName: "icon_good")
        } else {
            exteriorImage.image = #imageLiteral(resourceName: "icon_bad")
        }
    }

    func setFuel(level: Int) {
        if level <= 25 {
            highFuelView.isHidden = true
            mediumFuelView.isHidden = true
            lowFuelConstraint.constant = CGFloat(Float(level)*2.0)
        } else if level < 75 {
            highFuelView.isHidden = true
            mediumFuelConstraint.constant = CGFloat(Float(level - 25)*2.0)
        } else {
            highFuelConstraint.constant = CGFloat(Float(level - 75) * 2)
        }
    }
}
