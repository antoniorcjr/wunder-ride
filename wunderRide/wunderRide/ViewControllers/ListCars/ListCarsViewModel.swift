//
//  ListCarsViewModel.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 07/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import Foundation

struct ListCarsViewModel {
    var placemarks: [PlaceMarks] = []
    var filteredPlaceMarks: [PlaceMarks] = []
    var filters: Filters

    init(with resourcePath: String = "locations") {
        let request = Request()
        placemarks = request.requestPlaceMarks(with: resourcePath)
        filteredPlaceMarks = placemarks
        filters = Filters()
        applyFilter()
    }

    func numberOfItems() -> Int {
        return filteredPlaceMarks.count
    }

    func getName(for index: Int) -> String {
        if (filteredPlaceMarks.count) > index {
            return filteredPlaceMarks[index].name ?? ""
        } else {
            return ""
        }
    }

    func getAddress(for index: Int) -> String {
        if (filteredPlaceMarks.count) > index {
            return filteredPlaceMarks[index].address ?? ""
        } else {
            return ""
        }
    }

    func getInterior(for index: Int) -> String {
        if (filteredPlaceMarks.count) > index {
            return filteredPlaceMarks[index].interior?.rawValue ?? ""
        } else {
            return ""
        }
    }

    func getExterior(for index: Int) -> String {
        if (filteredPlaceMarks.count) > index {
            return filteredPlaceMarks[index].exterior?.rawValue ?? ""
        } else {
            return ""
        }
    }

    func getFuel(for index: Int) -> Int {
        return filteredPlaceMarks[index].fuel
    }

    mutating func applyFilter() {
        filteredPlaceMarks = placemarks.filter{
            return filters.interior.contains($0.interior?.rawValue ?? "") &&
                   filters.exterior.contains($0.exterior?.rawValue ?? "")
        }
    }

    mutating func setFilter(with newFilter: Filters) {
        filters = newFilter
        applyFilter()
    }
}
