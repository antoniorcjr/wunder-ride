//
//  ViewController.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 06/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import UIKit

class ListCarsViewController: UIViewController, FilterDelegate {
    var viewModel: ListCarsViewModel?

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ListCarsViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "listCarFilterSegue" {
            if let vc = segue.destination as? FilterViewController {
                vc.filters = viewModel?.filters
                vc.delegate = self
            }
        }
    }

    func didFinishFilter(with filter: Filters) {
        viewModel?.setFilter(with: filter)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension ListCarsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfItems() ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "carCell")
            as? CarTableViewCell else { return UITableViewCell() }

        cell.name.text = viewModel?.getName(for: indexPath.row)
        cell.address.text = viewModel?.getAddress(for: indexPath.row)
        cell.setInterior(value: viewModel?.getInterior(for: indexPath.row) ?? "")
        cell.setExterior(value: viewModel?.getExterior(for: indexPath.row) ?? "")
        cell.setFuel(level: viewModel?.getFuel(for: indexPath.row) ?? 0)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
}
