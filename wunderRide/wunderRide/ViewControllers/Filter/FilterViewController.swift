//
//  FilterViewController.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 08/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import UIKit

protocol FilterDelegate: class {
    func didFinishFilter(with filter: Filters)
}

class FilterViewController: UIViewController {
    var filters: Filters?
    let good = ConservationState.GOOD.rawValue
    let unacceptable = ConservationState.UNACCEPTABLE.rawValue

    weak var delegate: FilterDelegate? = nil

    @IBOutlet var interiorGoodButton: ButtonState!
    @IBOutlet var interiorUnacceptableButton: ButtonState!
    @IBOutlet var exteriorGoodButton: ButtonState!
    @IBOutlet var exteriorUnacceptableButton: ButtonState!

    override func viewDidLoad() {
        super.viewDidLoad()
        interiorGoodButton.setState(buttonState:
            filters?.interior.contains(good) ?? false)
        interiorUnacceptableButton.setState(buttonState: filters?.interior.contains(unacceptable) ?? false)
        exteriorGoodButton.setState(buttonState:
            filters?.exterior.contains(good) ?? false)
        exteriorUnacceptableButton.setState(buttonState:
            filters?.exterior.contains(unacceptable) ?? false)
    }

    override func didMove(toParentViewController parent: UIViewController?) {
        filters?.interior = []
        filters?.exterior = []

        if interiorGoodButton.buttonState {
            filters?.interior.append(good)
        }
        if interiorUnacceptableButton.buttonState {
            filters?.interior.append(unacceptable)
        }
        if exteriorGoodButton.buttonState {
            filters?.exterior.append(good)
        }
        if exteriorUnacceptableButton.buttonState {
            filters?.exterior.append(unacceptable)
        }

        delegate?.didFinishFilter(with: filters ?? Filters())
    }
}
