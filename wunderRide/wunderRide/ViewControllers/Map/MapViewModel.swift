//
//  MapViewModel.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 07/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import Foundation
import MapKit

struct MapViewModel {
    var placemarks: [PlaceMarks] = []
    let locationManager = CLLocationManager()
    var annotations: [MKAnnotation] = []

    init(with resourcePath: String = "locations" ) {
        let request = Request()
        placemarks = request.requestPlaceMarks(with: resourcePath)
    }

    func getUserLocation() -> CarAnnotation? {
        if let userLocation = locationManager.location?.coordinate {
            let annotation = CarAnnotation(coordinate: userLocation)
            annotation.isUser = true
            return annotation
        }
        return nil
    }

    mutating func getAnnotations() -> [MKAnnotation] {
        annotations = []
        for place in placemarks {
            if place.coordinates?.latitude != nil &&
                place.coordinates?.longitude != nil {
                let annotation = CarAnnotation(placeMark: place)
                annotations.append(annotation)
            }
        }

        return annotations
    }

    func annotationsToHide(unless annotation: MKAnnotation) -> [MKAnnotation] {
        let annotations = self.annotations.filter { $0 !== annotation }
        return annotations
    }
}
