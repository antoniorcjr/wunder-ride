//
//  MapViewController.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 07/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var mapView: MKMapView!

    var viewModel: MapViewModel?
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MapViewModel()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self

            // Saving power
            locationManager.startMonitoringSignificantLocationChanges()
            locationManager.pausesLocationUpdatesAutomatically = true

            // Set the activity type to help system determine when to pause updates
            locationManager.activityType = CLActivityType.otherNavigation
        }

        plotPlaces()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Asking for user Location authorization
        checkLocationAuthorizationStatus()
    }

    func checkLocationAuthorizationStatus() {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedWhenInUse:
            mapView.showsUserLocation = false
        case .denied, .restricted:
            let alert = UIAlertController(title: "Do you want to share your location with us?", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
                }
            }))
            self.present(alert, animated: true)
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            break
        }
    }

    func plotPlaces() {
        var annotations = viewModel?.getAnnotations() ?? []

        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(annotations)

        if let userLocation = viewModel?.getUserLocation() {
            annotations.append(userLocation)
        }

        mapView.showAnnotations(annotations, animated: true)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !CLLocationManager.significantLocationChangeMonitoringAvailable() {
            return
        }

        plotPlaces()
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            manager.stopMonitoringSignificantLocationChanges()
            return
        }
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.startMonitoringSignificantLocationChanges()
            mapView.showsUserLocation = true
            plotPlaces()
        }
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = view.annotation {
            let annotationsToHide = viewModel?.annotationsToHide(unless: annotation) ?? []
            mapView.removeAnnotations(annotationsToHide)
        }
    }

    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        let annotations = viewModel?.getAnnotations() ?? []

        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(annotations)
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = MKMarkerAnnotationView()
        guard let annotation = annotation as? CarAnnotation else {return nil}
        let identifier = "marker"

        if let dequedView = mapView.dequeueReusableAnnotationView(
            withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            annotationView = dequedView
        } else {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "marker")
            annotationView.canShowCallout = true
            annotationView.calloutOffset = CGPoint(x: -5, y: -5)
            annotationView.leftCalloutAccessoryView = UIImageView(image: #imageLiteral(resourceName: "icon_car_pin"))
        }

        if annotation.isUser {
            annotationView.glyphImage = #imageLiteral(resourceName: "icon_person")
        } else {
            annotationView.glyphImage = #imageLiteral(resourceName: "icon_car_pin")
        }

        return annotationView

    }
}
