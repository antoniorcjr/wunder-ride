//
//  CarAnnotation.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 07/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import Foundation
import MapKit

class CarAnnotation: NSObject, MKAnnotation {
    var placeMark: PlaceMarks?
    var userCoordinate: CLLocationCoordinate2D?
    var isUser: Bool = false

    var coordinate: CLLocationCoordinate2D {
        if let latitude = placeMark?.coordinates?.latitude,
           let longitude = placeMark?.coordinates?.longitude {
            let mark = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude), longitude: CLLocationDegrees(longitude))
            return mark
        } else if isUser {
            if let latitude = userCoordinate?.latitude, let longitude = userCoordinate?.longitude {
                let mark = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                return mark
            }
        }
        return CLLocationCoordinate2D()
    }

    init(placeMark: PlaceMarks) {
        self.placeMark = placeMark
        super.init()
    }

    init(coordinate: CLLocationCoordinate2D) {
        userCoordinate = coordinate
        super.init()
    }

    var title: String? {
        return placeMark?.name
    }

    var subtitle: String? {
        return placeMark?.address
    }
}
