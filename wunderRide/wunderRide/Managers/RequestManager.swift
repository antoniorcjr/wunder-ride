//
//  RequestManager.swift
//  wunderRide
//
//  Created by Antonio de Carvalho Jr on 06/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Request {
    func requestPlaceMarks(with resourcePath: String = "locations") -> [PlaceMarks] {
        var placemarks: [PlaceMarks] = []

        if let path = Bundle.main.path(forResource: resourcePath, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                let places = jsonObj["placemarks"].array ?? []
                for place in places {
                    placemarks.append(PlaceMarks(json: place))
                }
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }

        return placemarks
    }
}
