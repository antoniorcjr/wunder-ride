//
//  MapTests.swift
//  wunderRideTests
//
//  Created by Antonio de Carvalho Jr on 08/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import XCTest
import SwiftyJSON
import MapKit
@testable import wunderRide

class MapTests: XCTestCase {
    var viewModelCorrect: MapViewModel?
    var viewModelWithErrors: MapViewModel?
    
    override func setUp() {
        super.setUp()
        viewModelCorrect = MapViewModel(with: "locations-test-correct")
        viewModelWithErrors = MapViewModel(with: "locations-test-errors")
    }

    func test_getAnnotations_number() {
        let annotationsTestOne = viewModelCorrect?.getAnnotations()
        let annotationsTestTwo = viewModelWithErrors?.getAnnotations()

        XCTAssertEqual(annotationsTestOne?.count, 2)
        XCTAssertEqual(annotationsTestTwo?.count, 7)
    }

    func test_getAnnotations_not_nil() {
        let annotationsTestOne = viewModelCorrect?.getAnnotations() ?? []
        let annotationsTestTwo = viewModelWithErrors?.getAnnotations() ?? []

        for annotation in annotationsTestOne {
            XCTAssertNotNil(annotation)
        }

        for annotation in annotationsTestTwo {
            XCTAssertNotNil(annotation)
        }
    }

    func test_annotationsToHide() {
        let annotation = viewModelCorrect?.getAnnotations() ?? []
        let annotationsToHide = viewModelCorrect?.annotationsToHide(unless: annotation.first!)

        XCTAssertEqual(annotationsToHide?.count, 1)
    }

    func test_annotationsToHide_annotation_nil() {
        let wrongAnnotation = viewModelWithErrors?.getAnnotations() ?? []
        let annotationsToHide = viewModelCorrect?.annotationsToHide(unless: wrongAnnotation.first!)
        
        XCTAssertEqual(annotationsToHide?.count, 0)
    }
}
