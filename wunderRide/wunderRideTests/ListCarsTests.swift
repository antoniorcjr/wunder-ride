//
//  ListCarsTests.swift
//  wunderRideTests
//
//  Created by Antonio de Carvalho Jr on 08/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import wunderRide

class ListCarsTests: XCTestCase {
    var viewModelCorrect: ListCarsViewModel?
    var viewModelWithErrors: ListCarsViewModel?

    override func setUp() {
        super.setUp()
        viewModelCorrect = ListCarsViewModel(with: "locations-test-correct")
        viewModelWithErrors = ListCarsViewModel(with: "locations-test-errors")
    }

    func test_number_of_section() {
        let numberCorrect = viewModelCorrect?.numberOfItems()
        let numberErrors = viewModelWithErrors?.numberOfItems()

        XCTAssertEqual(numberCorrect, 2)
        XCTAssertEqual(numberErrors, 6)
    }

    func test_getName_correct_index() {
        let nameFirst = viewModelCorrect?.getName(for: 0)
        let nameSecond = viewModelCorrect?.getName(for: 1)

        XCTAssertEqual(nameFirst, "HH-GO8522")
        XCTAssertEqual(nameSecond, "HH-GO8480")
    }

    func test_getName_incorrect_value() {
        let name = viewModelWithErrors?.getName(for: 9)

        XCTAssertEqual(name, "")
    }

    func test_getName_incorrect_index() {
        let nameFailIndex = viewModelCorrect?.getName(for: 150)
        
        XCTAssertEqual(nameFailIndex, "")
    }

    func test_getAddress_correct_index() {
        let addressFirst = viewModelCorrect?.getAddress(for: 0)
        let addressSecond = viewModelCorrect?.getAddress(for: 1)

        XCTAssertEqual(addressFirst, "Lesserstraße 170, 22049 Hamburg")
        XCTAssertEqual(addressSecond, "Grosse Reichenstraße 7,20457 Hamburg")
    }

    func test_getAddress_incorrect_value() {
        let address = viewModelWithErrors?.getAddress(for: 9)
        
        XCTAssertEqual(address, "")
    }

    func test_getAddress_incorrect_index() {
        let addressFailIndex = viewModelCorrect?.getAddress(for: 150)

        XCTAssertEqual(addressFailIndex, "")
    }

    func test_getInterior_correct_index() {
        let interiorFirst = viewModelCorrect?.getInterior(for: 0)
        let interiorSecond = viewModelCorrect?.getInterior(for: 1)

        XCTAssertEqual(interiorFirst, "UNACCEPTABLE")
        XCTAssertEqual(interiorSecond, "GOOD")
    }

    func test_getInterior_incorrect_index() {
        let interiorFailIndex = viewModelCorrect?.getInterior(for: 150)
        
        XCTAssertEqual(interiorFailIndex, "")
    }

    func test_getExterior_correct_index() {
        let exteriorFirst = viewModelCorrect?.getExterior(for: 0)
        let exteriorSecond = viewModelCorrect?.getExterior(for: 1)

        XCTAssertEqual(exteriorFirst, "UNACCEPTABLE")
        XCTAssertEqual(exteriorSecond, "UNACCEPTABLE")
    }

    func test_getExterior_incorrect_index() {
        let exteriorFailIndex = viewModelCorrect?.getExterior(for: 150)

        XCTAssertEqual(exteriorFailIndex, "")
    }

    func test_apply_filter_unacceptable() {
        var filter = Filters()
        filter.interior = [ConservationState.UNACCEPTABLE.rawValue]
        filter.exterior = [ConservationState.UNACCEPTABLE.rawValue]
        viewModelCorrect?.setFilter(with: filter)

        XCTAssertEqual(viewModelCorrect?.numberOfItems(), 1)
    }

    func test_apply_filter_good() {
        var filter = Filters()
        filter.interior = [ConservationState.GOOD.rawValue]
        filter.exterior = [ConservationState.GOOD.rawValue]
        viewModelCorrect?.setFilter(with: filter)
        
        XCTAssertEqual(viewModelCorrect?.numberOfItems(), 0)
    }

    func test_apply_filter_mix() {
        var filter = Filters()
        filter.interior = [ConservationState.GOOD.rawValue, ConservationState.UNACCEPTABLE.rawValue]
        filter.exterior = [ConservationState.GOOD.rawValue, ConservationState.UNACCEPTABLE.rawValue]
        viewModelCorrect?.setFilter(with: filter)
        
        XCTAssertEqual(viewModelCorrect?.numberOfItems(), 2)
    }
}
