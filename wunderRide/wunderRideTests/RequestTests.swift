//
//  RequestTests.swift
//  wunderRideTests
//
//  Created by Antonio de Carvalho Jr on 07/09/18.
//  Copyright © 2018 Antonio de Carvalho Jr. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import wunderRide

class RequestTests: XCTestCase {
    var placemarksCorrect: [PlaceMarks] = []
    var placemarksErrors: [PlaceMarks] = []

    override func setUp() {
        super.setUp()

        if let path = Bundle.main.path(forResource: "locations-test-correct", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                let places = jsonObj["placemarks"].array ?? []
                for place in places {
                    placemarksCorrect.append(PlaceMarks(json: place))
                }
            } catch let error {
                XCTFail("parse error: \(error.localizedDescription)")
            }
        } else {
            XCTFail("File not found")
        }

        if let path = Bundle.main.path(forResource: "locations-test-errors", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                let places = jsonObj["placemarks"].array ?? []
                for place in places {
                    placemarksErrors.append(PlaceMarks(json: place))
                }
            } catch let error {
                XCTFail("parse error: \(error.localizedDescription)")
            }
        } else {
            XCTFail("File not found")
        }
    }

    func testJSONMappingCorrect() throws {
        let first = placemarksCorrect[0]
        XCTAssertEqual(first.address, "Lesserstraße 170, 22049 Hamburg")
        XCTAssertEqual(first.coordinates?.latitude, 10.07526)
        XCTAssertEqual(first.coordinates?.longitude, 53.59301)
        XCTAssertEqual(first.engineType, "CE")
        XCTAssertEqual(first.exterior, ConservationState(rawValue: "UNACCEPTABLE"))
        XCTAssertEqual(first.fuel, 42)
        XCTAssertEqual(first.interior, ConservationState(rawValue: "UNACCEPTABLE"))
        XCTAssertEqual(first.name, "HH-GO8522")
        XCTAssertEqual(first.vin, "WME4513341K565439")

        let second = placemarksCorrect[1]
        XCTAssertEqual(second.address, "Grosse Reichenstraße 7,20457 Hamburg")
        XCTAssertEqual(second.coordinates?.latitude, 9.99622)
        XCTAssertEqual(second.coordinates?.longitude, 53.54847)
        XCTAssertEqual(second.engineType, "CE")
        XCTAssertEqual(second.exterior, ConservationState(rawValue: "UNACCEPTABLE"))
        XCTAssertEqual(second.fuel, 45)
        XCTAssertEqual(second.interior, ConservationState(rawValue: "GOOD"))
        XCTAssertEqual(second.name, "HH-GO8480")
        XCTAssertEqual(second.vin, "WME4513341K412697")
    }

    func testJSONMappingIncorrectNotNil() throws {
        let len = placemarksErrors.count
        for place in placemarksErrors.prefix(len - 2) {
            XCTAssertNotNil(place.address)
            XCTAssertNotNil(place.coordinates)
            XCTAssertNotNil(place.engineType)
            XCTAssertNotNil(place.exterior)
            XCTAssertNotNil(place.fuel)
            XCTAssertNotNil(place.interior)
            XCTAssertNotNil(place.name)
            XCTAssertNotNil(place.vin)
        }
    }

    func testJSONMappingIncorrectNoValues() throws {
        let placeToTest = placemarksErrors[8]
        XCTAssertNotNil(placeToTest.address)
        XCTAssertNotNil(placeToTest.coordinates)
        XCTAssertNotNil(placeToTest.engineType)
        XCTAssertNotNil(placeToTest.exterior)
        XCTAssertNotNil(placeToTest.fuel)
        XCTAssertNotNil(placeToTest.interior)
        XCTAssertNotNil(placeToTest.name)
        XCTAssertNotNil(placeToTest.vin)
    }

    func testJSONMappingIncorrectNoKeys() throws {
        let placeToTest = placemarksErrors[9]
        XCTAssertNotNil(placeToTest.address)
        XCTAssertNotNil(placeToTest.coordinates)
        XCTAssertNotNil(placeToTest.engineType)
        XCTAssertNotNil(placeToTest.exterior)
        XCTAssertNotNil(placeToTest.fuel)
        XCTAssertNotNil(placeToTest.interior)
        XCTAssertNotNil(placeToTest.name)
        XCTAssertNotNil(placeToTest.vin)
    }
}
